<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class VideoCineController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function accueil()
    { 
        return $this->render('video_cine/index.html.twig', [
            'controller_name' => 'VideoCineController',
        ]);
    }

     /**
     * @Route("/", name="home")
     */
    public function home()
    { 
        return $this->render('video_cine/index.html.twig', [
            'controller_name' => 'VideoCineController',
        ]);
    }


}
